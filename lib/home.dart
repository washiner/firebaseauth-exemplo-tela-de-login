import 'package:flutter/material.dart';
import 'package:treina_app/pagina_Inicial.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("washiner"),
        centerTitle: true,
      ),
      body: Center(
        child: Container(
          child: FlutterLogo(size: 200),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context) => PaginaInicial()));
        }
      ),
      drawer: Drawer(

      ),
    );
  }
}
