import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:treina_app/home.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: _corApp(),
      home: Home(),
    );
  }
}

  ThemeData _corApp(){
   return ThemeData(
     primaryColor: Color(0xff7D79D0),
     accentColor: Color(0xff7b1fa2),
   );
  }

